module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    browserify: {
      js: {
        src: 'js/app.module.js',
        dest: 'build/js/app.module.js',
      },
    },

    watch: {
      js: {
        files: 'js/*',
        tasks: ['browserify', 'watch']
      },
      css: {
        files: 'styles/*',
        tasks: ['less', 'watch']
      },
      html: {
        files: 'view/*',
        tasks: ['copy', 'watch']
      },
      index: {
        files: 'index.html',
        tasks: ['injector', 'watch']
      }
    },

    less: {
      development: {
        files: {
          'build/styles/styles.css': 'styles/*.less'
        }
      }
    },

    copy: {
      view: {
        expand: true,
        src: ['view/*.html'],
        dest: 'build/',
      },
    },

    injector: {
      options: {
        template: 'index.html',
        destFile: 'build/index.html',
        ignorePath: 'build',
        addRootSlash: false
      },
      build: {
        files: {
          'build/index.html': ['build/js/*.js', 'build/styles/*.css'],
        }
      }
    },

    jshint: {
      all: ['js/*.js']
    },
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-injector');

  // Default task(s).
  grunt.registerTask('default', ['browserify', 'less', 'injector', 'copy', 'watch']);
  grunt.registerTask('js', ['jshint']);

};