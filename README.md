Repository for Angular training:

1) Initial version of calculator App

2) Added ng-repeat for buttons, created Grunt tasks, updated calculator service.

3) Added browserify, calculator directive and watch Grunt task.

4) Added backend support with Parse.com

DEMO: [http://orphy.ho.ua/angular-training/](http://orphy.ho.ua/angular-training/)

I'm using the following [Angular Style Guide](https://github.com/johnpapa/angular-styleguide)