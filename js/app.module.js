(function() {
    'use strict';
    var angular = require('angular');

    angular
        .module('app', []);

    require('./calc.controller');
    require('./calc.service');
    require('./calc.directive');
})();