(function() {
    'use strict';

    angular
        .module('app')
        .directive('calculatorComponent', calculatorComponent);

    function calculatorComponent() {
        return {
            restrict: 'E',
            templateUrl: 'view/calc.html',
            bindToController: true,
            controller: CalcDirectiveController,
            controllerAs: 'vm',
            scope: {
                results: '='
            }
        };
    }

    CalcDirectiveController.$inject = ['$scope', 'calcFactory'];

    function CalcDirectiveController($scope, calcFactory) {
        var vm = this;
        vm.title = 'CalcDirectiveController';
        
        vm.results = [];

        vm.setValue = setValue;
        vm.setOper = setOper;
        vm.reset = reset;
        vm.equals = equals;
        vm.operBtns = calcFactory.getOperBtns();
        vm.numBtns = calcFactory.getNumBtns();

        reset();
        activate();

        function activate() {
            calcFactory.getResultsFromParse().then(function(data) {
                vm.results = data || [];
                // Update the view when data is loaded
                $scope.$apply();
            });
        }

        /* Sets value that is being entered with digit buttons */
        function setValue(value) {
            vm.currValue += value;
            vm.display = vm.currValue;
        }

        /* Sets operator */
        function setOper(oper) {
            if (vm.currValue) {
                setNum();
            }

            vm.oper = oper;
            vm.display = vm.oper;
        }

        /* Sets temporary vars with entered values */
        function setNum() {
            if (!vm.firstNum) {
                vm.firstNum = parseFloat(vm.currValue);
            } else {
                if (vm.currValue) {
                    vm.secondNum = parseFloat(vm.currValue);
                } else {
                    vm.secondNum = vm.firstNum;
                }
                // Result of math. operation is saved in vm.firstNum
                vm.firstNum = calcFactory.calc(vm.oper, vm.firstNum, vm.secondNum);
                vm.secondNum = 0;
            }

            vm.currValue = '';
        }

        /* Resets all values */
        function reset() {
            vm.currValue = '';
            vm.firstNum = 0;
            vm.secondNum = 0;
            vm.oper = '';
            vm.display = '0';
        }

        /* Calculates final result */
        function equals() {
            setNum();
            vm.display = vm.firstNum;
            vm.results.push(vm.firstNum);
            calcFactory.sendResultsToParse(vm.results);
        }
    }
})();