(function() {
    'use strict';

    angular
        .module('app')
        .factory('calcFactory', calcFactory);

    function calcFactory() {
        // Initialize Parse
        Parse.initialize("ZTys7gqFDHazk0gPIc32M8cMWyLcVSiL2f2E2MBd", "H4YTi9VXVoOTWkWsg97ssd6zBluKXtbFgcC4z78p");

        var CalcObject = new Parse.Object.extend("CalcObject");
        var calcObject = new CalcObject();
        calcObject.id = 'c2OFOJr0A6';

        return {
            calc: function(oper, firstNum, secondNum) {
                switch (oper) {
                    case '+':
                        return firstNum + secondNum;
                    case '-':
                        return firstNum - secondNum;
                    case '*':
                        return firstNum * secondNum;
                    case '/':
                        return firstNum / secondNum;
                    // Returns the remainder of division
                    case '%':
                        return firstNum % secondNum;
                }
            },
            getOperBtns: function() {
                return [
                    {
                        class: 'btn btn-warning col-md-4',
                        oper: '%'
                    },
                    {
                        class: 'btn btn-warning col-md-4',
                        oper: '/',
                        symbol: '÷'
                    },
                    {
                        class: 'btn btn-warning col-md-4',
                        oper: '*',
                        symbol: '×'
                    },
                    {
                        class: 'btn btn-warning col-md-4',
                        oper: '-',
                    },
                    {
                        class: 'btn btn-warning col-md-4',
                        oper: '+',
                    }
                ];
            },
            getNumBtns: function() {
                return [
                    {
                        class: 'btn btn-info col-md-4',
                        value: '7'
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '8'
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '9'
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '4',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '5',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '6',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '1',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '2',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '3',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '0',
                    },
                    {
                        class: 'btn btn-info col-md-4',
                        value: '.',
                    }
                ];
            },
            getResultsFromParse: function() {
                var query = new Parse.Query(CalcObject);
                var promise = new Parse.Promise();
                query.get(calcObject.id, {
                    success: function(data) {
                        var results = data.get("result");
                        promise.resolve(results);
                    }
                });
                return promise;
            },
            sendResultsToParse: function(results) {
                calcObject.save({result: results});
            },
            removeResultsFromParse: function() {
                calcObject.unset('result');
                calcObject.save();
            }
        };
    }
})();