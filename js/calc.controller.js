(function() {
    'use strict';

    angular
        .module('app')
        .controller('CalcController', CalcController);

    CalcController.$inject = ['calcFactory'];

    function CalcController(calcFactory) {
        var vm = this;
        vm.title = 'CalcController';

        vm.removeResults = function() {
        	calcFactory.removeResultsFromParse();
        	vm.results = [];
        };
    }
})();